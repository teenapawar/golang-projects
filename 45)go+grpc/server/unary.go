package main

import (
	"context"
	pb "grpc-project/proto"
)

func (s *helloserver) SayHello(ctx context.Context, req *pb.NoParam) (*pb.HelloResponse, error){
	return &pb.HelloResponse{
		Message: "hello",
	},nil
}


