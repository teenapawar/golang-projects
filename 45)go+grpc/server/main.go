package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
	pb "grpc-project/proto"
)

const (
	port = ":8080"
)

type helloserver struct {
	pb.GreetServiceServer
	//pb.UnimplementedGreetServiceServer
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to start the server listen: %v", err)
	}
	grrpcServer := grpc.NewServer()
	pb.RegisterGreetServiceServer(grrpcServer, &helloserver{})
	log.Printf("server started at %v", lis.Addr())
	if err := grrpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to start :%v", err)
	}
}
