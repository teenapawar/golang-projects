package main

import (
	pb "grpc-project/proto"
	"io"
	"log"
	"time"
)

func SayHelloClientStreaming(stream pb.GreetService_SayHelloClientStreamingServer) error{
    var messages []string
	for{
		req,err:=stream.Recv()
		if err == io.EOF{
			return stream.SendAndClose(&pb.MessagesList{
				Messages: messages,
			})
		}
		if err !=nil{
			return err
		}
		log.Printf("got req with name: %v",req.Name)
		messages=append(messages, "hello",req.Name)
	}
}