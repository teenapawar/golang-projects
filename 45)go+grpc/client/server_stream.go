package main

import (
	"context"
	pb "grpc-project/proto"
	"io"
	"log"
	//"time"
)

func callSayHelloServerStream(client pb.GreetServiceClient,name *pb.NameList){
       log.Printf("streaming started")
	//    ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	//    defer cancel()
	//   stream,err:= client.SayHelloServerStreaming(ctx,name)
	stream,err:= client.SayHelloServerStreaming(context.Background(),name)
	  if err != nil {
		log.Fatalf("Could not greet: %v", err)
	}
	for{
		message,err:=stream.Recv()
		if err == io.EOF{
			break
		}
		if err != nil {
			log.Fatalf("err while streaming: %v", err)
		}
		log.Println(message)
	}
	log.Printf("streaming finished")
}