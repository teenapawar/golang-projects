package main

import (
	"fmt"
	"log"
	"net/http"
)

func main(){
	fileserver := http.FileServer(http.Dir("./static"))
	http.Handle("/",fileserver)
	http.HandleFunc("/form",formHandler)
	http.HandleFunc("/hello",helloHandler)
	fmt.Printf("starting port on 8080\n")
	if err:= http.ListenAndServe(":8080",nil); err !=nil{
		log.Fatal(err)
	}	
}

func helloHandler(w http.ResponseWriter, r *http.Request){
	if r.URL.Path !="/hello"{
		http.Error(w,"404 not found",http.StatusNotFound)
		return
	}
	if r.Method != "GET"{
		http.Error(w,"method not found",http.StatusNotFound)
		return
	}
	fmt.Fprintf(w,"say hi from hello handler")

}

func formHandler(w http.ResponseWriter, r *http.Request){
	if err := r.ParseForm();err !=nil{
		fmt.Fprintf(w,"Parseform()",err)
		return
	}
	fmt.Fprintf(w,"post req successful")
	name := r.FormValue("name")
	address:= r.FormValue("address")
	fmt.Fprintf(w,"name is %s\n",name)
	fmt.Fprintf(w,"address is %s",address)


}