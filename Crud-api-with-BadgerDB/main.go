package main

import (
	//"fmt"
	"fmt"
	"log"
	"os"

	badger "github.com/dgraph-io/badger/v3"
	//"golang.org/x/text/cases"
)

func main() {
	db, err := badger.Open(badger.DefaultOptions("tmp/badger"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	for {
		var choice string
		fmt.Println("enter you choice")
		fmt.Println("1.enter value in badger db")
		fmt.Println("2.display all records")
		fmt.Println("3.delete records")
		fmt.Println("4.update records")
		fmt.Println("5.exit")
		fmt.Scanln(&choice)
		var key, value string
		switch choice {
		case "1":
			fmt.Println("enter key and value")
			fmt.Scanln(&key, &value)
			InsertRecords(db, key, value)
		case "2":
			DisplayRecords(db)
		case "3":
			fmt.Println("enter key")
			fmt.Scanln(&key)
			DeleteRecords(db, key)
		case "4":
			fmt.Println("enter key which you want to update ")
			fmt.Scanln(&key)
			UpdateRecords(db, key)
		default:
			os.Exit(0)
		}
	}

}

func InsertRecords(db *badger.DB, key string, value string) error {
	// Start a writable transaction.
	txn := db.NewTransaction(true)
	defer txn.Discard()

	// Use the transaction...
	err := txn.Set([]byte(key), []byte(value))
	if err != nil {
		return err
	}

	// Commit the transaction and check for error.
	if err := txn.Commit(); err != nil {
		return err
	}
	fmt.Println("reocrd inserted")
	fmt.Println("-------------------------")
	return nil
}

func DisplayRecords(db *badger.DB) {
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			err := item.Value(func(v []byte) error {
				fmt.Printf("key=%s, value=%s\n", k, v)
				fmt.Println("-------------------------")
				return nil
			})
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}

func DeleteRecords(db *badger.DB, key string) {
	err := db.Update(func(txn *badger.Txn) error { // copy display code and chage view to update
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			//removing value part of display func
			if string(k) == key { //checking key
				err := txn.Delete(k)
				if err != nil {
					fmt.Println(err)
				} else {
					fmt.Println("record deleted successfully for key = ", key)
					fmt.Println("-------------------------")
					break
				}
			}

		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}

func UpdateRecords(db *badger.DB, key string) {
	err := db.Update(func(txn *badger.Txn) error { // copy display code and chage view to update
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()		//removing value part of display func
			if string(k) == key { //checking key
				fmt.Println("enter value")
				var updatevalue string
				fmt.Scanln(&updatevalue)
				err := txn.Delete(k)
				if err != nil {
					fmt.Println(err)
				}
				err = txn.Set([]byte(key), []byte(updatevalue))
				if err != nil {
					return err
				}
				fmt.Println("record updated successfully for key = ", key)
				fmt.Println("-------------------------")

			} else {
				fmt.Println("key not found")
			}

		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}
