package main

import (
	"encoding/json"
	"fmt"

	//"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type movie struct {
	ID       string   `json:"id"`
	Isbn     string   `json:"isbn"`
	Title    string   `json:"title"`
	Director Director `json:"director"`
}
type Director struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

var movies []movie

func main() {
	movies = append(movies, movie{ID: "1", Isbn: "1111", Title: "KPH", Director: Director{"abhay", "pawar"}})
	movies = append(movies, movie{ID: "2", Isbn: "2222", Title: "lll", Director: Director{"tau", "pawar"}})
	movies = append(movies, movie{ID: "3", Isbn: "3333", Title: "mmm", Director: Director{"antra", "pawar"}})
	movies = append(movies, movie{ID: "4", Isbn: "4444", Title: "nnn", Director: Director{"om", "pawar"}})
	movies = append(movies, movie{ID: "5", Isbn: "5555", Title: "ooo", Director: Director{"teena", "pawar"}})

	r := mux.NewRouter()
	r.HandleFunc("/movies", getMovies).Methods("GET")
	r.HandleFunc("/movies/{id}", getMovie).Methods("GET")
	r.HandleFunc("/movies", createMovies).Methods("POST")
	r.HandleFunc("/movies/{id}", updateMovies).Methods("PUT")
	r.HandleFunc("/movies/{id}", deleteMovies).Methods("DELETE")
	fmt.Println("listing on port 8000")
	http.ListenAndServe(":8000", r)

}

func getMovies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content.Type", "application/json")
	json.NewEncoder(w).Encode(movies)
	fmt.Println(movies)

}
func getMovie(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content.Type", "application/json")
	params := mux.Vars(r)
	for _, value := range movies {
		if value.ID == params["id"] {
			json.NewEncoder(w).Encode(value)
			break
		}
	}

}
func createMovies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content.Type", "application/json")
	var moviecreated movie
	_ = json.NewDecoder(r.Body).Decode(&moviecreated)
	moviecreated.ID = strconv.Itoa(rand.Intn(1000))
	movies = append(movies, moviecreated)
	fmt.Fprintf(w, "created successfully", http.StatusCreated)
	json.NewEncoder(w).Encode(movies) // or json.NewEncoder(w).Encode(movieCreated)

}
func updateMovies(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for index, value := range movies {
		if value.ID == params["id"] {
			movies = append(movies[:index], movies[index+1:]...) //delete from ore movies list
			var movieupdate movie
			_ = json.NewDecoder(r.Body).Decode(movieupdate) //decoded body which came
			movies = append(movies, movieupdate)
			json.NewEncoder(w).Encode(movieupdate)
			return
		}
	}

}
func deleteMovies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content.Type", "application/json")
	params := mux.Vars(r)
	for index, value := range movies {
		if value.ID == params["id"] {
			movies = append(movies[:index], movies[index+1:]...)
			break
		}
	}
	fmt.Fprintf(w, "deleted successfully", http.StatusOK)

}
